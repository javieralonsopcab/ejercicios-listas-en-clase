/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;
import ufps.util.colecciones_seed.ListaS;
/**
 *
 * @author Javier
 */
public class Test_Ordenamiento_Burbuja {
    
    
    public static void main(String [] args){
        ListaS lista = new ListaS<>();
        lista.insertarAlInicio(24524);
        lista.insertarAlInicio(46754);
        lista.insertarAlInicio(3524);
        lista.insertarAlInicio(5748);
        lista.insertarAlInicio(57896);
        lista.insertarAlInicio(797876);
        lista.insertarAlInicio(97545);
        
        System.out.println("La lista insertada: "+lista.toString());
        long e = System.nanoTime();
        lista.ordenarBurbuja();
        long time = System.nanoTime() -e;
        System.out.println("El ordenamiento de burbuja demoro: "+time+ " nanosegundos, en un intel Celeron");
        System.out.println("La lista ordenada mediante burbuja: "+lista.toString());
    }
    
    
}
